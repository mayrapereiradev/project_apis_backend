<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'created_at', 'updated_at'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

}
