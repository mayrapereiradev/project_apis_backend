<?php

namespace App\Http\Controllers\Api;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Order;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;    
    }

    public function index()
    {
        $clients = $this->client::all();

        return response()->json($clients, 200);
    }


    public function store(ClientRequest $request)
    {
        $data = $request->all();
        
        try {

        $client = $this->client->create($data);

           return response()->json([
                'data' => [
                    'msg' => 'Cliente cadastrado com sucesso.'
                ]   
            ], 200);

        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
    }

    public function show($id)
    {

    
        try {

           $client = $this->client->find($id);

           if($client)
           {

            return response()->json([
                'data' => [
                    'client' => $client,
                    'msg' => 'Cliente retornado com sucesso.'
                ]   
            ], 200);

           }else {

               return response()->json([
                'data' => [
                    'msg' => 'Não foi possível retornar o cliente'
                ]   
            ], 404);

           }

           
        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
    }


    public function update($id, ClientRequest $request)
    {
        $data = $request->all();
                
        try {

            $client = $this->client->find($id);



            if($client)
            {
 
                $client->update($data);

                return response()->json([
                     'data' => [
                         'msg' => 'Cliente alterado com sucesso.'
                     ]   
                 ], 200);

            } else {
 
                return response()->json([
                 'data' => [
                     'msg' => 'Não foi possível econtrar o cliente'
                 ]   
             ], 404);
 
            }

        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
    }

    public function destroy($id)
    {
        try {

            $client = $this->client->find($id);

            if($client)
            {
 
                $client->delete();

                return response()->json([
                    'data' => [
                        'msg' => 'Cliente deletado com sucesso.'
                    ]   
                ], 200);

            } else {
 
                return response()->json([
                 'data' => [
                     'msg' => 'Não foi possível econtrar o cliente'
                 ]   
             ], 404);
 
            }

        } catch (\Exception $e) {

            return response()->jason(['error' => $e->getMessage()], 401);

        }
        
    }
}
