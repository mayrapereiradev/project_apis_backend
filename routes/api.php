<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api')->group(function(){

    Route::prefix('clientes')->group(function(){

        Route::get('/', 'ClientController@index');
        Route::get('/show/{id}', 'ClientController@show');
        Route::post('/store', 'ClientController@store');
        Route::put('/update/{id}', 'ClientController@update');
        Route::delete('/destroy/{id}', 'ClientController@destroy');
    
    });

    Route::prefix('pedidos')->group(function(){

        Route::get('/', 'OrderController@index');
        Route::get('/show/{id}', 'OrderController@show');
        Route::post('/store', 'OrderController@store');
        Route::put('/update/{id}', 'OrderController@update');
        Route::delete('/destroy/{id}', 'OrderController@destroy');
        Route::get('/last', 'OrderController@lastOrders');
    
    });

});
